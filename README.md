# Estate Agent Commission Bid System

## Basic understanding of the system

## Homeowner
1. A homeowner would like to sell their property.
2. He will register/login.
3. He will list his properties as well as the property information
4. Confirm his property listing

## Estate agents
1. An estate agent would like to view properties
2. He will login / register
3. He can view properties listed in a certain area
4. He selects a desired property and makes a bid on it
His bid can include the services he offers eg: open house, magazine advertises etc.
As well as his fee price
Homeowners receive a list of bids from estate agents and selects the best offers from their
list of bids

## Team Members
- Raees Aziz Muhammad (raees@bbd.co.za)
- Kyle Schoonbee (kyles@bbd.co.za)
- Sashen Naidoo (sashenn@bbd.co.za)
- Ayesha Mohamed (ayesham@bbd.co.za)
- Brad Betts (bradleyb@bbd.co.za)
- Lebo Nzimande (lebohangn@bbd.co.za)

## Assigned Tasks
### Raees Aziz Muhammad
- Views

### Kyle Schoonbee
- User Defined Function

### Sashen Naidoo
- Stored Procedures

### Lebo Nzimande
- Stored Procedures

### Ayesha Mohomed 
- ERD 
- Initial Tables and Mock Values

### Brad Betts
- ERD
- User Defined Function with Kyle

**The whole team contributed to the discussion around the ERD and what tables should be used and how to structure the Database**









